
package tampilan;

import java.awt.HeadlessException;
import java.sql. *;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import koneksi.AppPerpusKoneksi;

/**
 *
 * @author aep
 */
public class FormBuku extends javax.swing.JFrame {
  private Connection conn = new AppPerpusKoneksi().connect();
  
  public FormBuku() {
    initComponents();
    datatable();
  }

  protected void datatable() {
    DefaultTableModel bukuTableModel = (DefaultTableModel) bukuTable.getModel();
    bukuTableModel.setRowCount(0);
    
    Object[] baris = {"judul", "penulis", "isi"};
    
    String sql = "SELECT * FROM buku";
    try {
      java.sql.Statement stat = conn.createStatement();
      ResultSet hasil;
      hasil = stat.executeQuery(sql);
      
      while(hasil.next()) {
        String judul   = hasil.getString("judul");
        String penulis = hasil.getString("penulis");
        String isi     = hasil.getString("isi");
        System.out.println(judul);
        
        bukuTableModel.addRow(new Object[]{judul, penulis, isi});
      }
    } catch (SQLException e) {
      System.err.println(e.getMessage());
    }
  }
  
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    formJudul = new javax.swing.JTextField();
    jLabel3 = new javax.swing.JLabel();
    formPenulis = new javax.swing.JTextField();
    jLabel4 = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    formIsi = new javax.swing.JTextArea();
    simpanBtn = new javax.swing.JButton();
    ubahBtn = new javax.swing.JButton();
    hapusBtn = new javax.swing.JButton();
    keluarBtn = new javax.swing.JButton();
    jScrollPane2 = new javax.swing.JScrollPane();
    bukuTable = new javax.swing.JTable();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
    jLabel1.setText("DATA BUKU");

    jLabel2.setText("Judul Buku");

    jLabel3.setText("Penulis Buku");

    jLabel4.setText("Isi");

    formIsi.setColumns(20);
    formIsi.setRows(5);
    jScrollPane1.setViewportView(formIsi);

    simpanBtn.setText("Simpan");
    simpanBtn.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        simpanBtnActionPerformed(evt);
      }
    });

    ubahBtn.setText("Ubah");

    hapusBtn.setText("Hapus");

    keluarBtn.setText("Keluar");
    keluarBtn.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        keluarBtnActionPerformed(evt);
      }
    });

    bukuTable.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][] {

      },
      new String [] {
        "Judul", "Penulis", "Isi"
      }
    ) {
      boolean[] canEdit = new boolean [] {
        false, false, false
      };

      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit [columnIndex];
      }
    });
    bukuTable.addMouseListener(new java.awt.event.MouseAdapter() {
      public void mouseClicked(java.awt.event.MouseEvent evt) {
        bukuTableMouseClicked(evt);
      }
    });
    jScrollPane2.setViewportView(bukuTable);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGap(267, 267, 267)
            .addComponent(jLabel1))
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
              .addGap(27, 27, 27)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel2)
                .addComponent(jLabel3)
                .addComponent(jLabel4))
              .addGap(36, 36, 36)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(formJudul)
                .addComponent(formPenulis)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)))
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
              .addGap(49, 49, 49)
              .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                  .addComponent(simpanBtn)
                  .addGap(64, 64, 64)
                  .addComponent(ubahBtn)
                  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(hapusBtn)
                  .addGap(34, 34, 34)
                  .addComponent(keluarBtn))))))
        .addContainerGap(22, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(32, 32, 32)
        .addComponent(jLabel1)
        .addGap(37, 37, 37)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2)
          .addComponent(formJudul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(18, 18, 18)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel3)
          .addComponent(formPenulis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(29, 29, 29)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel4)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(27, 27, 27)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(simpanBtn)
          .addComponent(ubahBtn)
          .addComponent(hapusBtn)
          .addComponent(keluarBtn))
        .addGap(18, 18, 18)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void keluarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keluarBtnActionPerformed
    dispose();
  }//GEN-LAST:event_keluarBtnActionPerformed

  public void kosong() {
    formJudul.setText("");
    formPenulis.setText("");
    formIsi.setText("");
  }
  
  private void simpanBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanBtnActionPerformed
    String sql = "insert into buku (judul, penulis, isi) values (?, ?, ?)";
    
    try {
      PreparedStatement stat = conn.prepareStatement(sql);
      stat.setString(1, formJudul.getText());
      stat.setString(2, formPenulis.getText());
      stat.setString(3, formIsi.getText());
      
      stat.executeUpdate();
      JOptionPane.showMessageDialog(null, "Data berhasil disimpan");
      datatable();
      kosong();
    } catch (HeadlessException | SQLException e) {
      System.err.println(e.getMessage());
    }
  }//GEN-LAST:event_simpanBtnActionPerformed

  private void bukuTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bukuTableMouseClicked
    int bar = bukuTable.getSelectedRow();
    
    String judul = bukuTable.getValueAt(bar, 0).toString();
    String penulis = bukuTable.getValueAt(bar, 1).toString();
    String isi = bukuTable.getValueAt(bar, 2).toString();
    
    formJudul.setText(judul);
    formIsi.setText(isi);
    formPenulis.setText(penulis);
  }//GEN-LAST:event_bukuTableMouseClicked

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(FormBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(FormBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(FormBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(FormBuku.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new FormBuku().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JTable bukuTable;
  private javax.swing.JTextArea formIsi;
  private javax.swing.JTextField formJudul;
  private javax.swing.JTextField formPenulis;
  private javax.swing.JButton hapusBtn;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JButton keluarBtn;
  private javax.swing.JButton simpanBtn;
  private javax.swing.JButton ubahBtn;
  // End of variables declaration//GEN-END:variables
}
