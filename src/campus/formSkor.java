/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus;

/**
 *
 * @author aep
 */
public class formSkor extends javax.swing.JFrame {
  
  int skorAValue = 0;
  int skorBValue = 0;
  
  public formSkor() {
    initComponents();
    timAButton.setEnabled(true);
    timBButton.setEnabled(true);
  }

  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    timASkor = new javax.swing.JLabel();
    timBSkor = new javax.swing.JLabel();
    timAButton = new javax.swing.JButton();
    timBButton = new javax.swing.JButton();
    hasilAkhirButton = new javax.swing.JButton();
    resetButton = new javax.swing.JButton();
    closeForm = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Form Skor");

    jLabel2.setText("Indonesia");

    jLabel3.setText("Argentina");

    timASkor.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    timASkor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    timASkor.setText("0");

    timBSkor.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    timBSkor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    timBSkor.setText("0");

    timAButton.setText("Tim A");
    timAButton.setEnabled(false);
    timAButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        timAButtonActionPerformed(evt);
      }
    });

    timBButton.setText("Tim B");
    timBButton.setEnabled(false);
    timBButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        timBButtonActionPerformed(evt);
      }
    });

    hasilAkhirButton.setText("Hasil Akhir");
    hasilAkhirButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        hasilAkhirButtonActionPerformed(evt);
      }
    });

    resetButton.setText("Reset");
    resetButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        resetButtonActionPerformed(evt);
      }
    });

    closeForm.setText("Close");
    closeForm.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        closeFormActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(66, 66, 66)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addGap(19, 19, 19)
            .addComponent(timASkor, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(timBSkor, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(29, 29, 29))
          .addComponent(hasilAkhirButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(timAButton)
              .addComponent(jLabel2))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(timBButton, javax.swing.GroupLayout.Alignment.TRAILING)))
          .addComponent(resetButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(closeForm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addGap(69, 69, 69))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addGap(27, 27, 27)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2)
          .addComponent(jLabel3))
        .addGap(18, 18, 18)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(timASkor)
          .addComponent(timBSkor))
        .addGap(18, 18, 18)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(timAButton)
          .addComponent(timBButton))
        .addGap(26, 26, 26)
        .addComponent(hasilAkhirButton)
        .addGap(18, 18, 18)
        .addComponent(resetButton)
        .addGap(18, 18, 18)
        .addComponent(closeForm)
        .addContainerGap(25, Short.MAX_VALUE))
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void timAButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timAButtonActionPerformed
    skorAValue++;
    timASkor.setText(Integer.toString(skorAValue));
  }//GEN-LAST:event_timAButtonActionPerformed

  private void timBButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_timBButtonActionPerformed
    skorBValue++;
    timBSkor.setText(Integer.toString(skorBValue));
  }//GEN-LAST:event_timBButtonActionPerformed

  private void hasilAkhirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hasilAkhirButtonActionPerformed
    timAButton.setEnabled(false);
    timBButton.setEnabled(false);
  }//GEN-LAST:event_hasilAkhirButtonActionPerformed

  private void resetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetButtonActionPerformed
    skorAValue = 0;
    skorBValue = 0;
    timASkor.setText(Integer.toString(skorAValue));
    timBSkor.setText(Integer.toString(skorBValue));
    timAButton.setEnabled(true);
    timBButton.setEnabled(true);
  }//GEN-LAST:event_resetButtonActionPerformed

  private void closeFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeFormActionPerformed
    dispose();
  }//GEN-LAST:event_closeFormActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(formSkor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(formSkor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(formSkor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(formSkor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new formSkor().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton closeForm;
  private javax.swing.JButton hasilAkhirButton;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JButton resetButton;
  private javax.swing.JButton timAButton;
  private javax.swing.JLabel timASkor;
  private javax.swing.JButton timBButton;
  private javax.swing.JLabel timBSkor;
  // End of variables declaration//GEN-END:variables
}
