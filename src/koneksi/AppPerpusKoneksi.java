
package koneksi;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;

public class AppPerpusKoneksi {
  private Connection koneksi;
  public Connection connect() {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      System.out.println("Koneksi berhasil");
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    
    String url = "jdbc:mysql://localhost/perpustakaan";
    
    try {
      koneksi = (Connection) DriverManager.getConnection(url, "root", "43p543pud1n!");
      System.out.println("Koneksi berhasil ke database");
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    
    return koneksi;
  }
  
}
